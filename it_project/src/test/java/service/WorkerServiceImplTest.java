package service;

import exeption.AlreadyExistException;
import exeption.NotFoundException;
import model.Worker;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import service.impl.WorkerServiceImpl;

import java.sql.SQLException;
import java.util.List;

public class WorkerServiceImplTest {

    private static WorkerService workerService;

    @BeforeAll
    static void init() {
        workerService = new WorkerServiceImpl();
    }

    @Test
    @DisplayName("Read all workers")
    public void readAllTest() throws SQLException {
        List<Worker> workerList = workerService.readAll();
        Assertions.assertTrue(workerList.size() > 0);
    }

    @Test
    @DisplayName("Read worker by id")
    public void readTest() throws NotFoundException, SQLException {
        int testId = 1;
        Worker expected = new Worker(testId, "Mykola Mykolenko", "jun", 600, 1);
        Worker actual = workerService.read(testId);
        Assertions.assertEquals(expected, actual);
    }

    @Test
    @DisplayName("Create new worker")
    public void createTest() throws SQLException, AlreadyExistException, NotFoundException {
        int id = 13;
        Worker workerToCreate = new Worker(id, "Test1", "test", 100);
        workerService.create(workerToCreate);
        Worker actual = workerService.read(id);
        Assertions.assertEquals(workerToCreate, actual);

        workerService.delete(id);
        Assertions.assertThrows(NotFoundException.class, () -> workerService.read(id));
    }

    @Test
    @DisplayName("Update worker")
    public void updateTest() throws NotFoundException, SQLException, AlreadyExistException {
        int id = 5;
        Worker previous = workerService.read(id);
        int updatedId = 111;
        Worker current = new Worker(updatedId, "Test5", "test5", 500,2);
        workerService.update(id, current);
        Assertions.assertEquals(current, workerService.read(updatedId));

        workerService.update(updatedId, previous);
        Assertions.assertEquals(previous, workerService.read(id));
    }
}
