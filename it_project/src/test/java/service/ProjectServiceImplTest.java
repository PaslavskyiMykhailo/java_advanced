package service;

import exeption.AlreadyExistException;
import exeption.NotFoundException;
import model.Project;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import service.impl.ProjectServiceImpl;

import java.sql.SQLException;
import java.util.List;

public class ProjectServiceImplTest {

    private static ProjectService projectService;


    @BeforeAll
    static void init() {
        projectService = new ProjectServiceImpl();
    }

    @Test
    @DisplayName("Read all projects")
    public void readAllTest() throws SQLException {
        List<Project> projectList = projectService.readAll();
        Assertions.assertTrue(projectList.size() > 0);
    }

    @Test
    @DisplayName("Read project by id")
    public void readTest() throws NotFoundException, SQLException {
        int testId = 1;
        Project expected = new Project(testId, "Webstore", 10000.0);
        Project actual = projectService.read(testId);
        Assertions.assertEquals(expected, actual);
    }

    @Test
    @DisplayName("Create new project")
    public void createTest() throws SQLException, AlreadyExistException, NotFoundException {
        int id = 13;
        Project projectToCreate = new Project(id, "Test1", 1000.1);
        projectService.create(projectToCreate);
        Project actual = projectService.read(id);
        Assertions.assertEquals(projectToCreate, actual);

        projectService.delete(id);
        Assertions.assertThrows(NotFoundException.class, () -> projectService.read(id));
    }

    @Test
    @DisplayName("Update project")
    public void updateTest() throws NotFoundException, SQLException, AlreadyExistException {
        int id = 4;
        Project previous = projectService.read(id);
        int updatedId = 111;
        Project current = new Project(updatedId, "TestName", 100.11);
        projectService.update(id, current);
        Assertions.assertEquals(current, projectService.read(updatedId));

        projectService.update(updatedId, previous);
        Assertions.assertEquals(previous, projectService.read(id));
    }
}
