package service;

import exeption.AlreadyExistException;
import exeption.NotFoundException;
import model.Project;
import model.TeamLead;
import org.junit.jupiter.api.*;
import service.impl.ProjectServiceImpl;
import service.impl.TeamLeadServiceImpl;

import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.List;

public class TeamLeadServiceImplTest {

    private static TeamLeadService teamLeadService;
    private static ProjectService projectService;

    @BeforeAll
    static void init(){
        teamLeadService = new TeamLeadServiceImpl();
        projectService = new ProjectServiceImpl();
    }
    @Test
    @DisplayName("Read all teamLeads")
    public void readAllTest () throws SQLException {
        List<TeamLead>teamLeadList =teamLeadService.readAll();
        Assertions.assertTrue(teamLeadList.size() >= 1);
    }

    @Test
    @DisplayName("Read teamLead by id ")
    public void readTest() throws NotFoundException, SQLException {
        int testId = 1;
        TeamLead expected = new TeamLead(testId,"Ivan Ivanenko", Timestamp.valueOf("1997-10-12 03:00:00"),3500);
        TeamLead actual = teamLeadService.read(testId);
        Assertions.assertEquals(expected,actual);
    }

    @Test
    @DisplayName("Update teamLead")
    @Disabled
    public void updateTest(){
    }

    @Test
    @DisplayName("Create and delete new teamLead")
    public void createAndDeleteTest() throws SQLException, AlreadyExistException, NotFoundException {
        int id = 13;

        //create test project
        Project projectToCreate = new Project(id,"TestProject",100.11);
        projectService.create(projectToCreate);
        Project projectActual = projectService.read(id);
        Assertions.assertEquals(projectToCreate,projectActual);

        //create test teamLead
        TeamLead teamLeadToCreate = new TeamLead(id,"TestTeamLead",Timestamp.valueOf("1990-10-10 00:00:00"),3000);
        teamLeadService.create(teamLeadToCreate);
        TeamLead teamLeadActual = teamLeadService.read(id);
        Assertions.assertEquals(teamLeadToCreate.getId(),teamLeadActual.getId());
        Assertions.assertEquals(teamLeadToCreate.getFullName(),teamLeadActual.getFullName());
        Assertions.assertEquals(teamLeadToCreate.getDateOfBirth().getDate(),teamLeadActual.getDateOfBirth().getDate());
        Assertions.assertEquals(teamLeadToCreate.getSalary(),teamLeadActual.getSalary());

        //remove test teamLead
        teamLeadService.delete(id);
        Assertions.assertThrows(NotFoundException.class, () -> teamLeadService.read(id));

        //remove test project
        projectService.delete(id);
        Assertions.assertThrows(NotFoundException.class, () ->  projectService.read(id));

    }

}
