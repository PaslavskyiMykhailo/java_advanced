package util;

public class SQLConstants {

    public static final String GET_ALL_PROJECTS = "SELECT * FROM company.project";
    public static final String GET_PROJECT_BY_ID = "SELECT * FROM company.project WHERE id = ?";
    public static final String INSERT_PROJECT = "INSERT INTO company.project (id, name, budget) VALUES (?, ?, ?)";
    public static final String UPDATE_PROJECT = "UPDATE company.project SET id = ?, name = ?, budget = ? WHERE (id = ?)";
    public static final String DELETE_PROJECT_BY_ID = "DELETE FROM company.project WHERE id = ?";

    public static final String GET_ALL_WORKERS = "SELECT * FROM company.worker";
    public static final String GET_WORKER_BY_ID = "SELECT * FROM company.worker WHERE id = ?";
    public static final String INSERT_WORKER = "INSERT INTO company.worker (id, full_name, title, salary) VALUES (?, ?, ?, ?)";
    public static final String UPDATE_WORKER = "UPDATE company.worker SET id = ?, full_name = ?, title = ?, salary = ?, team_lead_id = ? WHERE (id = ?)";
    public static final String DELETE_WORKER_BY_ID = "DELETE FROM company.worker WHERE id = ?";

    public static final String GET_ALL_TEAM_LEADS = "SELECT * FROM company.team_lead";
    public static final String GET_TEAM_LEAD_BY_ID = "SELECT * FROM company.team_lead WHERE id = ?";
    public static final String INSERT_TEAM_LEAD = "INSERT INTO company.team_lead (id, purchase_date) VALUES (?, ?)";
    public static final String UPDATE_TEAM_LEAD = "UPDATE company.team_lead SET full_name = ?, date_of_birth = ?, salary = ? WHERE (id = ?)";
    public static final String DELETE_TEAM_LEAD_BY_ID = "DELETE FROM company.team_lead WHERE id = ?";

}
