package dao;

import model.Worker;
import shared.AbstractCrudOperations;

public interface WorkerDao extends AbstractCrudOperations<Worker> {
}
