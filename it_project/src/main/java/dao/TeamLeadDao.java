package dao;

import model.TeamLead;
import shared.AbstractCrudOperations;

public interface TeamLeadDao extends AbstractCrudOperations<TeamLead> {
}
