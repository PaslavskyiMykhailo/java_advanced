package dao;

import model.Project;
import shared.AbstractCrudOperations;

public interface ProjectDao extends AbstractCrudOperations<Project> {
    }
