package model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class Project {
    private int id;
    private String name;
    private double budget;

    public Project(String name, double budget) {
        this.name = name;
        this.budget = budget;
    }
}

