package model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Timestamp;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class TeamLead {


    private int id;
    private String fullName;
    private Timestamp dateOfBirth;
    private int salary;

    /*public TeamLead(int id, String fullName, Timestamp dateOfBirth, int salary) {
        this.id = id;
        this.fullName = fullName;
        this.dateOfBirth = dateOfBirth;
        this.salary = salary;
    }*/

    /*public TeamLead(String fullName, Timestamp dateOfBirth, int salary) {
        this.fullName = fullName;
        this.dateOfBirth = dateOfBirth;
        this.salary = salary;
    }*/

}

