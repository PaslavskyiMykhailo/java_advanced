package model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Worker {

    private int id;
    private String fullName;
    private String title;
    private int salary;
    private int teamLeadId;

    public Worker(int id, String fullName, String title, int salary) {
        this.id = id;
        this.fullName = fullName;
        this.title = title;
        this.salary = salary;
    }

    public Worker(String fullName, String title, int salary) {
        this.fullName = fullName;
        this.title = title;
        this.salary = salary;
    }
}
