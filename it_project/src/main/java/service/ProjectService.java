package service;

import model.Project;
import shared.AbstractCrudOperations;

public interface ProjectService extends AbstractCrudOperations<Project> {
}
