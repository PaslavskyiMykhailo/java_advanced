package service;

import model.Worker;
import shared.AbstractCrudOperations;

public interface WorkerService extends AbstractCrudOperations<Worker> {
}
