package service;

import model.TeamLead;
import shared.AbstractCrudOperations;

public interface TeamLeadService extends AbstractCrudOperations<TeamLead> {
}
