package service.impl;

import dao.TeamLeadDao;
import dao.impl.TeamLeadDaoImpl;
import exeption.AlreadyExistException;
import exeption.NotFoundException;
import model.TeamLead;
import org.apache.log4j.Logger;
import service.TeamLeadService;

import java.sql.SQLException;
import java.util.List;

public class TeamLeadServiceImpl implements TeamLeadService {

    private TeamLeadDao teamLeadDao;
    private static final Logger logger = Logger.getLogger(TeamLeadServiceImpl.class);

    public TeamLeadServiceImpl() {
        teamLeadDao = new TeamLeadDaoImpl();
    }

    @Override
    public List<TeamLead> readAll() throws SQLException {
        logger.info("Read all teamleads request");
        return teamLeadDao.readAll();
    }

    @Override
    public TeamLead read(int id) throws SQLException, NotFoundException {
        TeamLead teamLead = teamLeadDao.read(id);
        if (teamLead == null) {
            logger.error("TeamLead with id " + id + " not found");
            throw new NotFoundException("TeamLead with id " + id + " not found");
        } else {
            logger.info("Getting TeamLead with id " + id);
            logger.info(teamLead);
            return teamLead;
        }
    }

    @Override
    public void create(TeamLead teamLead) throws SQLException, AlreadyExistException {
        if (isExists(teamLead.getId())) {
            logger.error("TeamLead with id " + teamLead.getId() + " already exists and can`t be created");
            throw new AlreadyExistException("TeamLead with id " + teamLead.getId() + "already exists");
        } else {
            logger.info("Creating teamLead " + teamLead);
            teamLeadDao.create(teamLead);
        }
    }

    @Override
    public void update(int id, TeamLead current) throws SQLException, NotFoundException, AlreadyExistException {
        if (isExists(id)) {
            if (!isExistsByName(current.getFullName())) {
                teamLeadDao.update(id, current);
                logger.info("TeamLead with id " + id + " was updated by teamLead " + current);
            } else {
                logger.error("TeamLead with name " + current.getFullName() + "already exists and can't be updated");
                throw new AlreadyExistException("TeamLead with name " + current.getFullName() + "already exists");
            }
        } else {
            logger.error("TeamLead with id " + id + "not found and can't be updated");
            throw new NotFoundException("TeamLead with id " + id + " not found");
        }
    }

    @Override
    public void delete(int id) throws SQLException, NotFoundException {
        if (!isExists(id)) {
            logger.error("TeamLead with id " + id + " not found and can't be deleted");
            throw new NotFoundException("TeamLead with id " + id + " not found");
        } else {
            teamLeadDao.delete(id);
            logger.info("TeamLead with id " + id + "was deleted");
        }
    }

    private List<TeamLead> getAll() throws SQLException {
        return teamLeadDao.readAll();
    }

    private boolean isExists(int teamLeadId) throws SQLException {
        boolean flag = false;
        for (TeamLead teamLead : getAll()) {
            if (teamLead.getId() == teamLeadId) {
                flag = true;
                break;
            }
        }
        return flag;
    }

    private boolean isExistsByName(String teamLeadName) throws SQLException {
        boolean flag = false;
        for (TeamLead teamLead : getAll()) {
            if (teamLead.getFullName() == teamLeadName) {
                flag = true;
                break;
            }
        }
        return flag;
    }
}
